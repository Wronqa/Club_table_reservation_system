export const image = '../images/club.jpg'
export const config = {
  name: 'my-map',
  areas: [
    {
      id: '1',
      title: 'Table 1 - Super Vip',
      shape: 'rect',
      name: '1',
      fillColor: '#eab54d4d',
      strokeColor: 'crimson',
      coords: [80, 90, 160, 130],
    },
    {
      id: '2',
      title: 'Table 2 - 6 persons',
      shape: 'rect',
      name: '2',
      fillColor: '#eab54d4d',
      strokeColor: 'crimson',
      coords: [153, 50, 193, 90],
    },
    {
      id: '3',
      title: 'Table 2 - 6 persons',
      shape: 'rect',
      name: '2',
      fillColor: '#eab54d4d',
      strokeColor: 'crimson',
      coords: [190, 50, 223, 90],
    },
    {
      id: '4',
      title: 'Table 2 - 6 persons',
      shape: 'rect',
      name: '2',
      fillColor: '#eab54d4d',
      strokeColor: 'crimson',
      coords: [230, 50, 263, 90],
    },
  ],
}
