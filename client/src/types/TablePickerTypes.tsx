import { CustomArea } from 'react-img-mapper'

export type Area = CustomArea & {
  title?: string
}
